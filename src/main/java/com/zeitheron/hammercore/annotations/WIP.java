package com.zeitheron.hammercore.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Marks class as WORK IN PROGRESS
 */
@Retention(RUNTIME)
public @interface WIP
{
}