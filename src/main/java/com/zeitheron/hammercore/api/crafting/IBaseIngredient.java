package com.zeitheron.hammercore.api.crafting;

import com.zeitheron.hammercore.api.crafting.impl.EnergyIngredient;
import com.zeitheron.hammercore.api.crafting.impl.FluidStackIngredient;
import com.zeitheron.hammercore.api.crafting.impl.MCIngredient;
import com.zeitheron.hammercore.api.crafting.impl.ODIngredient;

/**
 * A base class for any ingredient. <br>
 * The default implementations are: <br>
 * {@link EnergyIngredient}, {@link FluidStackIngredient}, {@link MCIngredient},
 * and {@link ODIngredient}
 */
public interface IBaseIngredient
{
}