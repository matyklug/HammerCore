package com.zeitheron.hammercore.internal.blocks.base;

import net.minecraft.block.properties.PropertyDirection;

public interface IBlockOrientable
{
	public static final PropertyDirection FACING = PropertyDirection.create("facing");
}