package com.zeitheron.hammercore.internal.blocks;

import net.minecraft.item.ItemBlock;

/**
 * Used for registering blocks to add a custom ItemBlock
 */
public interface IItemBlock
{
	public ItemBlock getItemBlock();
}