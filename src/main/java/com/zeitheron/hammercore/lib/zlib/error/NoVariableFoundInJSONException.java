package com.zeitheron.hammercore.lib.zlib.error;

public class NoVariableFoundInJSONException extends Error
{
	private static final long serialVersionUID = -4499873605296238001L;

	public NoVariableFoundInJSONException(String msg)
	{
		super(msg);
	}
}
