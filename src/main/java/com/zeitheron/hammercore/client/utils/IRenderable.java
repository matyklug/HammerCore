package com.zeitheron.hammercore.client.utils;

public interface IRenderable
{
	void render();
}