package com.zeitheron.hammercore.utils;

public enum EnumMoonPhase
{
	FULL, //
	WANING_GIBBOUS, //
	LAST_QUARTER, //
	WANING_CRESCENT, //
	NEW_MOON, //
	WAXING_CRESCENT, //
	FIRST_QUARTER, //
	WAXING_GIBBOUS;
}